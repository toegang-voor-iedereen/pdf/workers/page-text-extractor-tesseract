from langdetect import detect
from language_map import langdetect_to_tesseract


def detect_language(text: str):
    """
    Detects the language of a string

    Args:
        - text (str): the text to detect the language for

    Returns:
        - str: the tesseract compatible language code or unknown if not found
    """
    try:
        lang = detect(text)
        return langdetect_to_tesseract.get(lang, "unknown")

    except Exception:  # pragma: no cover
        return "unknown"
