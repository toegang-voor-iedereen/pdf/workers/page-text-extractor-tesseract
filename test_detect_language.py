from detect_language import detect_language


def test_detect_language():
    assert detect_language("Hallo ik ben Nederlands") == "nld"
    assert detect_language("Hallo ich bin Deutsch") == "deu"
    assert detect_language("Hi I am English") == "eng"
