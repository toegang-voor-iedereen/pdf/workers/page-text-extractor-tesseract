import unittest
from unittest.mock import MagicMock, patch
from main import job_handler


class TestJobHandler(unittest.TestCase):
    @patch("main.Image.open")
    @patch("main.parse")
    def test_job_handler_success(self, mock_parse, mock_image_open):
        # Mock dependencies
        mock_log = MagicMock()
        mock_publish = MagicMock()
        mock_image = MagicMock()
        mock_image_open.return_value = mock_image

        # Simulated parse output
        mock_parse.return_value = ("Extracted Content", 0.95)

        # Call the function
        job_handler(
            log=mock_log,
            job={},
            job_id=1,
            local_file_path="test_image.png",
            remote_file_storage=None,
            publish=mock_publish,
        )

        # Assertions for logging
        mock_log.info.assert_any_call("Opening image")
        mock_log.info.assert_any_call("Closing image")
        mock_log.info.assert_any_call("Job done")

        # Assertions for Image operations
        mock_image_open.assert_called_once_with("test_image.png")
        mock_image.close.assert_called_once()

        # Assertions for parse
        mock_parse.assert_called_once_with(mock_image, mock_log)

        # Assertions for publish
        mock_publish.assert_called_once_with(
            "contentResult", "Extracted Content", success=True, confidence=0.95
        )


if __name__ == "__main__":
    unittest.main()
