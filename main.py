import sys
import os
from PIL import Image
from kimiworker import Worker, KimiLogger
from parse import parse

numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", 1))


def job_handler(
    log: KimiLogger, job, job_id, local_file_path, remote_file_storage, publish
):
    """Handle getting page text from page image"""

    log.info("Opening image")
    image = Image.open(local_file_path)

    content_result, mean_confidence = parse(image, log)

    log.info("Closing image")
    image.close()

    log.info("Job done")

    publish(
        "contentResult",
        content_result,
        success=True,
        confidence=mean_confidence,
    )


if __name__ == "__main__":
    try:  # pragma: no cover
        worker = Worker(job_handler, "worker-page-interpreted-content-ocr", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:  # pragma: no cover
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
