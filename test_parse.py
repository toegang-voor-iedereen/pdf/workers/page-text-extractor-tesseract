import pytest
from unittest.mock import Mock, patch
from PIL import Image
from tesserocr import PT, RIL
from parse import parse


@pytest.fixture
def mock_api():
    with patch("parse.PyTessBaseAPI") as MockAPI:
        api = Mock()
        MockAPI.return_value = api
        api.GetUTF8Text.return_value = "Sample text"
        api.MeanTextConf.return_value = 90
        api.Version.return_value = "4.0.0"
        yield api


@pytest.fixture
def mock_image():
    img = Mock(spec=Image.Image)
    img.size = (100, 100)
    return img


@pytest.fixture
def mock_iterator():
    iterator = Mock()
    word = Mock()
    word.GetUTF8Text.return_value = "word"
    word.Confidence.return_value = 95
    word.BoundingBox.return_value = (10, 10, 20, 20)
    word.IsAtFinalElement.return_value = True

    text_line = Mock()
    text_line.GetUTF8Text.return_value = "Sample line"
    text_line.Confidence.return_value = 90
    text_line.BoundingBox.return_value = (5, 5, 25, 25)
    text_line.BlockType.return_value = PT.FLOWING_TEXT
    text_line.RowAttributes.return_value = {"row_height": 15}

    def mock_iterate_level(it, level):
        if level == RIL.TEXTLINE:
            return [text_line]
        return [word]

    with patch("parse.iterate_level", side_effect=mock_iterate_level):
        yield iterator


def test_empty_text(mock_api, mock_image):
    mock_api.GetUTF8Text.return_value = ""
    result, confidence = parse(mock_image)
    assert result == []
    assert confidence == 0


def test_successful_parse(mock_api, mock_image, mock_iterator):
    mock_api.GetIterator.return_value = mock_iterator

    result, confidence = parse(mock_image)

    assert confidence == 90
    assert len(result) == 1
    line = result[0]
    assert line["classification"] == "application/x-nldoc.element.text+line"
    assert line["confidence"] == 90
    assert line["attributes"]["text"] == "Sample line"
    assert len(line["children"]) == 1
    assert line["labels"][0]["name"] == "flowing_text"


def test_with_logger(mock_api, mock_image, mock_iterator):
    mock_logger = Mock()
    mock_api.GetIterator.return_value = mock_iterator

    result, confidence = parse(mock_image, mock_logger)

    mock_logger.info.assert_called_with("Getting text from image")
    assert mock_logger.child.called


@pytest.mark.parametrize(
    "block_type,expected_label",
    [
        (PT.HEADING_TEXT, "heading_text"),
        (PT.PULLOUT_TEXT, "pullout_text"),
        (PT.EQUATION, "equation"),
        (PT.INLINE_EQUATION, "inline_equation"),
        (PT.TABLE, "table"),
        (PT.VERTICAL_TEXT, "vertical_text"),
        (PT.CAPTION_TEXT, "caption_text"),
        (PT.FLOWING_IMAGE, "flowing_image"),
        (PT.HEADING_IMAGE, "heading_image"),
        (PT.PULLOUT_IMAGE, "pullout_image"),
        (PT.HORZ_LINE, "horz_line"),
        (PT.VERT_LINE, "vert_line"),
        (PT.NOISE, "noise"),
        (PT.COUNT, "count"),
    ],
)
def test_block_types(mock_api, mock_image, mock_iterator, block_type, expected_label):
    mock_api.GetIterator.return_value = mock_iterator

    with patch("parse.iterate_level") as mock_iterate:
        text_line = Mock()
        text_line.GetUTF8Text.return_value = "Test text"
        text_line.Confidence.return_value = 90
        text_line.BoundingBox.return_value = (5, 5, 25, 25)
        text_line.BlockType.return_value = block_type
        text_line.RowAttributes.return_value = {"row_height": 15}

        def mock_iterate_impl(it, level):
            if level == RIL.TEXTLINE:
                return [text_line]
            return []

        mock_iterate.side_effect = mock_iterate_impl

        result, _ = parse(mock_image)

        assert result[0]["labels"][0]["name"] == expected_label


def test_no_iterator(mock_api, mock_image):
    mock_api.GetIterator.return_value = None
    result, confidence = parse(mock_image)
    assert result == []
    assert confidence == 90
