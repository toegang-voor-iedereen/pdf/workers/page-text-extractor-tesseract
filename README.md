# Extract Page Interpreted Content OCR

This python3 worker extracts content from page images using Tesserocr.

## Running this worker locally

To run this worker locally on an MacOS device follow the following steps:

- Run `brew install conda`
- Run `export PATH="/opt/homebrew/anaconda3/bin:$PATH"`
- Run `brew install tesseract`
- Run `conda install -c conda-forge tesserocr`
- Run `pip install -r requirements.txt`
- Run `AMQP_HOST="127.0.0.1" AMQP_PORT=5672 AMQP_USER="YOUR_AMQP_USER" AMQP_PASS="YOUR_AMQP_PASS" EXCHANGE="page-interpretedcontent" MINIO_HOST="127.0.0.1" MINIO_PORT=9000 MINIO_ACCESS_KEY="YOUR_MINIO_ACCESS_KEY" MINIO_SECRET_KEY="YOUR_MINIO_SECRET_KEY" MINIO_USE_SSL="False" python3 main.py`

Please note it is important to install tesserocr through Conda, as a regular pip install won't work.
