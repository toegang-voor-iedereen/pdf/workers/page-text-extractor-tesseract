# Changelog

## [1.14.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.14.2...1.14.3) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr docker tag to v1.14.2 ([46e4a75](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/46e4a7513d4b343eaefb6b40378c55cd7e2f40a5))

## [1.14.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.14.1...1.14.2) (2025-03-05)


### Bug Fixes

* merge branch 'develop' into 'renovate/tesserocr-2.x' ([f8d1c7d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/f8d1c7d86776bb86434f14c607f8bce7941b559a))

## [1.14.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.14.0...1.14.1) (2025-01-20)


### Bug Fixes

* update base worker ([d5c52a7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/d5c52a76933a282d4d9f1d51cf2c256fa4fbf1a9))

# [1.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.13.1...1.14.0) (2024-12-16)


### Features

* **deps:** kimiworker 4.4.0, removed minio dependency ([ab535aa](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/ab535aa7ad48c40a220eeb8aeaeeca77b7e10a41))

## [1.13.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.13.0...1.13.1) (2024-12-02)


### Bug Fixes

* don't crash on empty pages ([a1bb5f5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/a1bb5f5d4e6f15ef8734940136a9e4de9f526af3))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.12.0...1.13.0) (2024-11-19)


### Features

* force release with kimiworker v3.6.0 ([14fb253](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/14fb2535804a1029e8112811839777f1431a5ef1))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.11.2...1.12.0) (2024-11-13)


### Features

* force new release as 1.11.2 appears to not have a package ([c6af62b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/c6af62b9311f6a9c888d7e37d71c1583af20668b))

## [1.11.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.11.1...1.11.2) (2024-11-12)


### Bug Fixes

* revert to ContentDefinition ([bf1e920](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/bf1e92058389d571a01c626e3052f7a440b01cdc))

## [1.11.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.11.0...1.11.1) (2024-11-06)


### Bug Fixes

* use old ContentDefinition with new kimiworker ([a0c981c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/a0c981cd4ca144c928dea5227e4691e4f721c7e8))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.10.0...1.11.0) (2024-10-22)


### Features

* use kimiworker 2.15.0 with nldocspec 4.1.1 ([df98147](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/df98147c127a2211099c9eab93cfa3e627ca0dc8))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.9.0...1.10.0) (2024-10-03)


### Features

* implemented kimiworker 2.7.0, using nldocspec 4.1.1 ([ebf6132](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/ebf61320f336db1a426840d172a0efaafb4f6197))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.8.1...1.9.0) (2024-09-11)


### Features

* added volume mount for /tmp ([073e906](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/073e90602de0311f6765fc9062ecf39d7a9e373d))

## [1.8.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.8.0...1.8.1) (2024-09-05)


### Bug Fixes

* version was not updated by CI ([f934116](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/f9341163ba4e449d26b40ae1e75fd2c7782f92d7))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.7.0...1.8.0) (2024-09-04)


### Features

* added securityContext to container and initContainers ([aa92194](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/aa9219474f8bf3d933796d8d1cd179ac6297d2cd))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.6.0...1.7.0) (2024-08-28)


### Features

* use new kimiworker to connect to station 4.0.0 and up ([1d9c6a3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/1d9c6a32841759f6a02b2d7de7a82c6ec9e32002))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.5.0...1.6.0) (2024-06-28)


### Features

* support initContainers in helm values ([6d72891](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/6d7289171626153a1ac49cddc352b23c06892fd0))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.4.0...1.5.0) (2024-06-05)


### Features

* support initContainers in helm values ([0f2c98a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/0f2c98a01d9eda498d00fcd375716e29dc291c4b))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/compare/1.3.0...1.4.0) (2024-06-04)


### Features

* correct worker name in chart ([c45ecc0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr/commit/c45ecc05f62fcd9cc628dfc2acdce6dbb7ee9965))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/compare/1.2.1...1.3.0) (2024-05-14)


### Bug Fixes

* remove path ([f873593](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/f873593fec5523558e87e8e754e9066fd782a72d))


### Features

* download languages ([6004c70](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/6004c70403bf55c72a977ab297fdf861cc3393ca))

## [1.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/compare/1.2.0...1.2.1) (2024-05-13)


### Bug Fixes

* remove bogus test ([ba32482](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/ba32482df0dd2a2d062b5710626535ee1e20a6bd))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/compare/1.1.0...1.2.0) (2024-05-06)


### Bug Fixes

* compatibility ([f805388](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/f805388b04473599ff5413dd9ee64f4c6f3814f3))


### Features

* kimiworker 1.1 ([a1515f6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/a1515f6f5bff7f55d8f216a2cf137a9ab84566d1))
* prepare for automated releases and tests ([39e3cc4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/39e3cc473fa933e18b886acbde4f3a77898a138e))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/compare/v1.0.0...1.1.0) (2024-04-24)


### Bug Fixes

* add pytest ([0e82e4a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/0e82e4a9bc71ffd7fe30133c7ed43473dda30984))
* force a release with the new pipeline ([a610459](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/a6104598b5928f801113e4518b4e32abc0b104af))
* install gcc and tesseract before ci test ([71bf090](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/71bf090e714067c149ef4690bd8f3a2aee926477))
* use shared ci without hard url ([21557b2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/21557b20bbc3136d8fb0f4cba011e9aa42083025))


### Features

* add tests for bbox method ([096c68e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-page-interpreted-content-ocr/commit/096c68e865f725b2eaa55f0a81a6085d29113cbe))
