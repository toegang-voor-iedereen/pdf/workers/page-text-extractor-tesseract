from typing import List, Optional
from PIL.Image import Image
from tesserocr import PSM, PT, RIL, PyTessBaseAPI, iterate_level

from kimiworker import convert_bounding_box, ContentDefinition, KimiLogger

from detect_language import detect_language


def parse(image: Image, log: Optional[KimiLogger] = None):
    image_width, image_height = image.size
    api = PyTessBaseAPI()
    api.Init(psm=PSM.AUTO_OSD, lang="nld")
    api.SetImage(image)
    api.AnalyseLayout()
    api.Recognize()

    page_text = api.GetUTF8Text()
    if not page_text or page_text == "":
        return [], 0

    mean_confidence = api.MeanTextConf()

    if log is not None:
        log.info("Getting text from image")

    lang_string = detect_language(page_text)

    if log is not None:
        log.child({"data": {"language": lang_string}}).info(
            f"Detected page language: {lang_string}"
        )

    content_result: List[ContentDefinition] = []

    iterator = api.GetIterator()
    if iterator:
        for text_line_index, text_line in enumerate(
            iterate_level(iterator, RIL.TEXTLINE)
        ):
            text_line_text = text_line.GetUTF8Text(RIL.TEXTLINE).strip()
            if text_line_text == "" or text_line_text.isspace():
                continue  # pragma: no cover
            text_line_confidence = text_line.Confidence(RIL.TEXTLINE)
            text_line_bounding_box = text_line.BoundingBox(RIL.TEXTLINE)
            text_line_block_type = text_line.BlockType()
            text_line_labels: list[str] = []
            match text_line_block_type:
                case PT.FLOWING_TEXT:  # Text that lives inside a column.
                    text_line_labels.append("flowing_text")
                case PT.HEADING_TEXT:  # Text that spans more than one column.
                    text_line_labels.append("heading_text")
                case PT.PULLOUT_TEXT:  # Text that is in a cross-column pull-out region.
                    text_line_labels.append("pullout_text")
                case PT.EQUATION:  # Partition belonging to an equation region.
                    text_line_labels.append("equation")
                case PT.INLINE_EQUATION:  # Partition has inline equation.
                    text_line_labels.append("inline_equation")
                case PT.TABLE:  # Partition belonging to a table region.
                    text_line_labels.append("table")
                case PT.VERTICAL_TEXT:  # Text-line runs vertically.
                    text_line_labels.append("vertical_text")
                case PT.CAPTION_TEXT:  # Text that belongs to an image.
                    text_line_labels.append("caption_text")
                case PT.FLOWING_IMAGE:  # Image that lives inside a column.
                    text_line_labels.append("flowing_image")
                case PT.HEADING_IMAGE:  # Image that spans more than one column.
                    text_line_labels.append("heading_image")
                case (
                    PT.PULLOUT_IMAGE
                ):  # Image that is in a cross-column pull-out region.
                    text_line_labels.append("pullout_image")
                case PT.HORZ_LINE:  # Horizontal Line.
                    text_line_labels.append("horz_line")
                case PT.VERT_LINE:  # Vertical Line.
                    text_line_labels.append("vert_line")
                case PT.NOISE:  # Lies outside of any column.
                    text_line_labels.append("noise")
                case PT.COUNT:  # Count
                    text_line_labels.append("count")

            text_line_row_attributes = text_line.RowAttributes()
            text_line_children: list[ContentDefinition] = []
            # Get words in line
            for word_index, word in enumerate(iterate_level(text_line, RIL.WORD)):
                word_text = word.GetUTF8Text(RIL.WORD).strip()
                word_confidence = word.Confidence(RIL.WORD)
                word_bounding_box = word.BoundingBox(RIL.WORD)
                text_line_children.append(
                    {
                        "classification": "application/x-nldoc.element.text+word",
                        "confidence": word_confidence,
                        "bbox": convert_bounding_box(
                            *word_bounding_box,
                            image_width=image_width,
                            image_height=image_height,
                        ),
                        "attributes": {
                            "text": word_text,
                            "wordIndex": word_index,
                            "origin": f"Tesseract {api.Version()}",
                        },
                        "children": [],
                        "labels": [],
                    }
                )
                if word.IsAtFinalElement(RIL.TEXTLINE, RIL.WORD):
                    break

            content_result.append(
                {
                    "classification": "application/x-nldoc.element.text+line",
                    "bbox": convert_bounding_box(
                        *text_line_bounding_box,
                        image_width=image_width,
                        image_height=image_height,
                    ),
                    "confidence": text_line_confidence,
                    "attributes": {
                        "text": text_line_text,
                        "lineIndex": text_line_index,
                        "lineHeight": text_line_row_attributes.get("row_height"),
                        "origin": f"Tesseract {api.Version()}",
                    },
                    "labels": [
                        {"name": name, "confidence": text_line_confidence}
                        for name in text_line_labels
                    ],
                    "children": text_line_children,
                }
            )

    return content_result, mean_confidence
