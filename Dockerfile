# syntax=docker/dockerfile:1

FROM python:3.13-slim

RUN apt-get update && apt-get -y install gcc g++ tesseract-ocr libtesseract-dev libleptonica-dev pkg-config wget

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN [ "/bin/bash", "-c", "wget https://github.com/tesseract-ocr/tessdata/raw/main/{osd,nld,deu,fra,eng}.traineddata -N -P tessdata" ]
ENV TESSDATA_PREFIX='./tessdata'

COPY . .

CMD ["python3", "-u", "main.py"]